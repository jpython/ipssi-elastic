#!/usr/bin/env bash

data=$( base64 -w 0 ../ssh-keys/id_rsa_aws_int.pub )
sed -e "s/^PUBKEY.*/${data}/" < node_init.tmpl.sh > node_init.sh

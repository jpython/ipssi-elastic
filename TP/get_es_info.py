#!/usr/bin/env python3

# TP "blanc" : récupérer nom du cluster ES et version

import requests

eshost = 'localhost'

with requests.get(f'http://{eshost}:9200') as r:
    if r.ok:
        data = r.json()
        name, version = data['cluster_name'], data['version']['number']
        print(f'Cluster name: {name}')
        print(f'ES version: {version}')

#!/usr/bin/python3

from movies import app as application

if __name__ == "__main__":
    application.run()

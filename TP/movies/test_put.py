#!/usr/bin/env python3

# Modèle acquisition/enregistrement vers ES

import requests
from datetime import datetime
import json 
from uuid import uuid4

eshost = 'localhost'

def log_query(date, ip, actor, theme):
    url = f'http://{eshost}:9200/movieslog/_doc/{uuid4()}'
    print(url)
    doc = { "actor":'test',
            "client_addr": '1.2.3.4', 
            'theme':'Harrison Ford',
             # '2020-06-04T15:36:30.017844'
            'date':datetime.now().isoformat(),
           }
    print(json.dumps(doc))
    r = requests.put(url, json=doc)
    if r.ok:
        print('put ok')
    else:
        print(r.status_code, r.reason)
    url = f'http://{eshost}:9200/movieslog/_doc/'
    print(url)
    r = requests.post(url, json=doc)
    if r.ok:
        print('post ok')
    else:
        print(r.status_code, r.reason)


if __name__ == '__main__':
    log_query(1,2,3,4)

#!/usr/bin/env python3

# Modèle acquisition/enregistrement vers ES

import requests

eshost = 'localhost'

def get_movies_info(actor=None, theme=None):
    '''Renvoie les infos sur les films répondant au critères'''
    if not any([actor, theme]):
        raise ValueError('Must provide at least actor or theme')
    url = f'http://{eshost}:9200/_search?size=10000'
    filters = []
    if actor:
        filters.append( { "match_phrase": { "fields.actors": actor} } )
    if theme:
        filters.append( { "match_phrase": { "fields.plot": theme } } )
    query = { "query":{
                 "bool": {
                           "must": filters,
                 }
              }
            }
    with requests.get(url,json=query) as r:
        if r.ok:
            data = r.json()
            ans = []
            for document in data['hits']['hits']:
                movie = document['_source']['fields']
                title, year, plot = movie.get('title'), movie.get('year'), \
                                     movie.get('plot')
                ans.append([title, year, plot])
        else:
            raise requests.exceptions.RequestException(f'Request not ok: {r.status_code} {r.reason}')

    return ans

if __name__ == '__main__':
    print('Harrison Ford', '/ love')
    print(*( m[0] for m in get_movies_info('Harrison Ford', 'love')),
            sep='\n')
    print()
    print('Harrison Ford', '/ Jones')
    print(*( m[0] for m in get_movies_info('Harrison Ford', 'Jones')),
            sep='\n')
    print()
    print(' / Star Wars')
    print(*( m[0] for m in get_movies_info(theme='Star Wars')),
            sep='\n')
    print()
    print(' / Galaxy')
    print(*( m[0] for m in get_movies_info(theme='Star Wars')),
            sep='\n')
    print('Harrison Ford /')
    print(*( m[0] for m in get_movies_info(actor='Harrison Ford')),
            sep='\n')
    print()

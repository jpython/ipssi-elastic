#!/usr/bin/env python3

from flask import Flask, request, render_template, \
                  redirect

from model import get_movies_info
from logmovies import log_query
from datetime import datetime

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

@app.route('/', methods=['POST'])
def search():
    actor = request.form['actor'].strip()
    theme = request.form['theme'].strip()
    d = {} 
    log_query(datetime.utcnow(), request.remote_addr , actor, theme)
    if not any([actor, theme]):
        d['error'] = 42
    else:
        movies = get_movies_info(actor,theme)
        movies = [ [ fld if fld else '' for fld in movie ] for movie in movies ]
        d['movies'] = movies 
    d['actor'] = actor
    d['theme'] = theme
    return render_template('index.html', **d)

if __name__ == '__main__':
    app.run()

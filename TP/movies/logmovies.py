#!/usr/bin/env python3

# Modèle acquisition/enregistrement vers ES

import requests
from datetime import datetime

eshost = 'localhost'

def log_query(date, ip, actor, theme):
    doc = { "actor": actor,
            "client_addr": ip, 
            'theme': theme,
            'date':date.isoformat(),
           }
    url = f'http://{eshost}:9200/movieslog/_doc/'
    r = requests.post(url, json=doc)
    return(r.ok)

if __name__ == '__main__':
    print(log_query(datetime.now(), '1.2.3.4', 'John Wayne', ''))

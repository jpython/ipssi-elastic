# Faire des sauvegarde l'index dans Elastic

Quand on est en "vrai" cluster, les données sont réparties
(et dupliquée) parmis plusieurs serveurs. Il faut que ces
serveurs (machines, VMs, conteneurs) dispose d'un volume
monté avec accès en écriture pour tous. 

Typiquement on configure un serveur de fichier (NFS) qui
exporte un répertoire et le monte sur chaque noeud du 
cluster.

(il y a d'autres possibilités : AWS S3, et autres solution Cloud)

On prévient le cluster de l'existence de ce volume :

~~~~
PUT /_snapshot/my_backup
{
  "type": "fs",
  "settings": {
    "location": "/srv/es-backup",
    "compress": true
  }
}
~~~~

Sauvegarder (snapshot) de tous les index :

~~~~
PUT /_snapshot/my_backup/all_2020060501
~~~~

Snapshot de certains index seulement :

~~~~
PUT /_snapshot/my_backup/snapshot_1
{
  "indices": "newmovies,movies2",
  "ignore_unavailable": true,
  "include_global_state": false
}
~~~~

Vérifier l'état d'un snapshot :

~~~~
GET /_snapshot/my_backup/all_2020060501
~~~~

Restauration :
~~~~
POST /_snapshot/my_backup/all_2020060501/_restore
    {
    "indices": "newmovies",
    "ignore_unavailable": true,
    "include_global_state": true
}
~~~~


# Spécification du TP

## Partie sur 15pts

Dans votre langage de programmation favori (PHP, JS, Python, Java, ...)
développer un site Web constitué d'un formulaire avec les champ 
de saisies suivants :
- un champ 'acteur' (par exemple Harrison Ford)
- un champ 'theme' (par example Stars)
- un bouton de validation

En réponse à la validation, la page affichée montre les résultats
des films correspondant, uniquement les champs titre, année et résumé (plot)

Fournir : code source, copie d'écran d'une recherche. Si vous utilisez
un framework merci de bien spécifier quels fichiers contient le code
que vous avez développé.

## Partie sur 5 pts

- Définissez en json un mapping pour un nouvel index 'movieslog' 
destiné à enregistrer l'activité de notre moteur de recherche, il
contiendra des champs comme l'ip du client et les valeurs saisies
dans les champs de saisie du formulaire
- Compléter votre solution de la première partie afin que ces
  information soit enregistrées dans l'index movieslog lors de
  la valudation du formulaire
- Vous pouvez aussi enregistrer d'autre infos comme :
  * date/heure de la recherche
  * navigateur (user agent) utilisé

Fournir : code source comme précédemment, fichier json du mapping,
contenu de l'index movieslog (json) après quelques tests.


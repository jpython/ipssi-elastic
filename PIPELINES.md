# Les pipelines et LogStash

- Sources de données (par ex. des logs)
- Pipeline : entrée -> traitement(s) (dissect, grok, geoip, ...) -> sortie
- Logstash permet de définir ces pipelines
- Si le fichier n'est pas local : agent beats qui envoie des données à logstach (voire à es)
- Pour lire un fichier : filebeats
- Il en existe d'autre (par ex. pour surveiller un base MySQL/MariaDB)
- Ils ont une faible empreinte disque/ram/cpu/..., généralement écrits en Go (alias de GoLang de Google)
- L'essentiel de Kubernetes est écrit en Go 

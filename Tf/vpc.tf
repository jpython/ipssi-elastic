resource "aws_vpc" "vpc_main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.vpc_main.id
}

resource "aws_route" "wan_access" {
  route_table_id          = aws_vpc.vpc_main.main_route_table_id
  destination_cidr_block  = "0.0.0.0/0"
  gateway_id              = aws_internet_gateway.default.id
}

resource "aws_subnet" "subnet-a" {
  vpc_id                  = aws_vpc.vpc_main.id
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true
}

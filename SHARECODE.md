# Préparer un backend Elastic pour ShareCode

l'appli ShareCode est là : https://framagit.org/jpython/share-code

Elle permet, comme le site de Google codeshare.io de partager du code
sur le Web facilement.

Sa version actuelle utilise le système de fichier pour enregister les
bouts de code partagé (répertoire/fichier).

Ça ne passerait certainement pas à l'échelle, comme codeshare.io qui
est souvent indisponible.

On pourrait stocker les bout de code dans un cluster Elastic et des
infos en plus (ip des utilisateurs, dates, user-agent, etc.)

On a déjà évoqué l'idée d'utiliser PostgreSQL.

Elastic serait aussi très intéressant (surtout à cause de la partie
cluster) d'autant que les données ne sont pas de nature très "relationnelles".

Proposer un mapping pour un index stockant les activité des posteurs
de code, un autre pour les activité des lecteurs de code.


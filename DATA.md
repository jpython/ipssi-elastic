# Architecture d'Elastic Search

- Base de donnée client/serveur en grappe (cluster)
- Données dupliquéee ET réparties (shard = éclat)
- Backend de recherche : moteur Lucene (fondation Apache)
- Élément d'un cluster :
  - Noeuds (serveurs ou conteneurs) exécutant ElasticSearch
  - Un noeud exécute Kibana (interface Web)
  - Un ou plusieurs noeud exécutant optionnelement LogStach (agrégateur de logs)
  - Des agents _beat_ (ex: filebeat) sur les conteneurs ou machines
    qui collecte de l'information et les envoie à ES directement ou à
    LogStash pour les décomposer avec Grok
- Indexation des données orienté recherche en texte libre (indexation des
  mots)
- Non seulement les données sont réparties sur les noeuds mais les
  requètes sont aussi exécutées de façon répartie
- Supporte naturellement de très grand jeux de données
- Administration et requêtage sous forme de call (API REST) HTTP/JSON

# Modèle de données

- Pas besoin de déclaration de structure (modèle) : déterminé à l'import
- Cependant il est recommandé de fournir un modèle (mapping) pour ne
  pas indexer tous les mots de tous les champs
- Lexique SQL/Elastic :
  * Base de données = index
  * Schéma = mapping
  * Table = Type
  * Enregistrement = Document
  * Colonne = Champ (ou colonne)
  * Requète SQL = Call HTTP/JSON
  * SELECT = Call HTTP GET
  * UPDATE/INSERT = Call HTTP PUT

Exercice : Importation de données et premières requètes :

https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4474691-etudiez-le-fonctionnement-d-elasticsearch

Note: si l'import du mapping échoue avec une problème de stucture JSON
(bizarre... à explorer) il faut adapter le call pour l'import du mapping:

~~~~
curl -XDELETE localhost:9200/movies2

curl -X PUT 'localhost:9200/movies2?include_type_name=true' -H"Content-Type: application/json" -d @mapping_movies/mapping.json

curl -XPUT -H "Content-Type: application/json" localhost:9200/_bulk --data-binary @mapping_movies/movies_elastic2.json
~~~~

Interroger l'état du cluster :

~~~~
curl https://localhost:9200/_cat
curl https://localhost:9200/_cat/indices
curl https://localhost:9200/_cluster/health/?pretty 
~~~~

L'état de nos index est 'yellow' : si un des noeuds tombe on perdrait
des données (normal : il n'y a qu'un seul noeud !)

Jusqu'à maintenant nos requètes étaient simples et étaient décrites
entièrement dans l'URL du call GET. Pour des requètes plus complexes
il faut une description en JSON :

Exercice : requètes complexes

https://openclassrooms.com/fr/courses/4462426-maitrisez-les-bases-de-donnees-nosql/4474696-interrogez-des-donnees-textuelles

Pour la mise au point de requêtes (entre autres) on peut installer kibana,
sur une installation locale il suffit de télécharger le binaire et
d'exécuter le script de lancement (ou installer un paquet debian ou rpm,
ou déployer des conteneurs docker). Kibana est accessible sur
http://localhost:5601/. Il possible de le sécuriser nativement (ssl + auth)
ou de le mettre derrière un proxy NGINX (cf. AWS.md).
 
On peut exécuter du "pseudo SQL" et le traduire en JSON :

~~~~
POST /_sql/
{
    "query": "SELECT fields.title,fields.rating FROM movies WHERE fields.title LIKE '%Wars%'",
    "fetch_size": 10
}

POST /_sql/translate
{
    "query": "SELECT fields.title,fields.rating FROM movies WHERE fields.title LIKE '%Wars%'",
    "fetch_size": 10
}
~~~~

Utilise pour la mise au point de requêtes complexes, il faut souvent "nettoyer" un peu le JSON obtenu.

